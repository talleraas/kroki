# What is Kroki?

Kroki is a GTK based picture playlist application written in Vala.

# Building Kroki

Instructions on how to build the latest version of Kroki. These can be modified to build a specific release.

This instruction only works on Linux based systems.

## Step one:

Install the following package:

- Gnome builder

## Step two:

`git clone https://codeberg.org/christoffer/kroki`

## Step three:

Open Gnome builder, then use "open project", go to the home directory and choose the folder/project named "kroki".
